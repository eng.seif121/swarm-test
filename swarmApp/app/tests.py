# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.test import TestCase
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from .models import Message
import ast


class MyTest(APITestCase):

    def setUp(self):
        new_user = User.objects.create_user(username='seif', password='test1234')
        token = Token.objects.create(user_id=1, key="2155223")
        print("--ok--")
    def test_token_route(self):

        # all_users = User.objects.all()
        # print(all_users)
        # print(new_user.id)
        data = {"username": "seif", "password": "test1234"}
        response = self.client.post('/api-token/', data, format='json')
        self.token = response.data['token']
        print("token :", self.token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_msg(self):
        new_user = User()
        # print("ID :",new_user.id)
        # print(new_user.authtoken_token.key)

        tokens = Token.objects.all()
        # print(token.id)
        data = {"title": "my msg", "body": "my msg body"}
        header = {'HTTP_AUTHORIZATION': 'Token {}'.format(tokens[0].key)}
        response = self.client.post('/messages/', data, **header)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

