from django.utils.deprecation import MiddlewareMixin
from views import key, cipher_suite


class CustomMiddleWare(MiddlewareMixin):
    def process_request(self, request):
        try:
            print('token : ', request.META['HTTP_AUTHORIZATION'])
            x = request.META['HTTP_AUTHORIZATION'].split(" ")[1]
            token = cipher_suite.decrypt(x)
            request.META['HTTP_AUTHORIZATION'] = 'JWT {}'.format(token)
        except:
            print('no token')

    def process_response(self, request, response):
        print "process_response executed"
        return response