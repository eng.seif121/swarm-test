# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models.signals import post_save, pre_save
from django.db import models
from django.dispatch import receiver
from django_mysql.models import JSONField, Model


class Message(Model):
    title = models.CharField(max_length=50)
    body = models.CharField(max_length=100)
    queues = JSONField(null=True)


@receiver(post_save, sender=Message)
def do_after_save(sender, instance, **kwargs):
    print "new save occurs"
    print sender.body

@receiver(pre_save, sender=Message)
def do_before_save(sender, instance, **kwargs):
    print("new save will occur now !!")